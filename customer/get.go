package customer

import (
	"encoding/json"
	"net/http"
	data "powergen/crm/data"

	//uuid "powergen/uuid"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/muhoro/log"
)

type Language struct {
	Id       string
	Language string
}
type customerResp struct {
	CustomerID string   `json:"customer_id"`
	Errors     []string `json:"errors"`
}
type CustomerResp struct {
	first_name string `json:"first_name"`
	last_name  string `json:"last_name"`
	surname    string `json:"surname"`
	house      string `json:"house"`

	identification_number string `json:"identification_no"`
	phone_number          string `json:"phone_number"`
	customer_id           string `json:"customer_id"`
}

type CustomersUpdate struct {
	FirstName string
	LastName  string
	Surname   string
	Address   string

	Identification string
	PrimaryPhone   string
	AlternatePhone string
}

func FetchCustomers(w http.ResponseWriter, r *http.Request) {

	b := []data.Customer{}
	limitStr := r.URL.Query().Get("limit")
	offsetStr := r.URL.Query().Get("offset")
	offset, limit := 0, 100

	if len(offsetStr) > 0 {
		var err error
		if offset, err = strconv.Atoi(offsetStr); err != nil {
			log.Error(err.Error(), nil)
			http.Error(w, "Invalid offset parameter", http.StatusBadRequest)
			return
		}
	}

	if len(limitStr) > 0 {
		var err error
		if limit, err = strconv.Atoi(limitStr); err != nil {
			log.Error(err.Error(), nil)
			http.Error(w, "Invalid limit parameter", http.StatusBadRequest)
			return
		}
	}
	db := r.Context().Value("database").(*gorm.DB)

	//	db.Limit(limit).Offset(offset).Joins("left join customers on language.id = customers.language").Select("customers.id, customers.first_name,customer.last_name,customer.surname,customer.identification_no,customer.primary_phone,customer.gender,language.language").Scan(&b)
	db.Limit(limit).Offset(offset).Find(&b)

	w.Header().Set("Content-Type", "Application/json")
	json.NewEncoder(w).Encode(b)
}

func GetCustomerSearch(w http.ResponseWriter, r *http.Request) {
	resp := []data.Customer{}
	// var cust = CustomerResp{}
	db := r.Context().Value("database").(*gorm.DB)
	search := strings.Title(r.URL.Query().Get("search"))
	if search != "" {
		db.Where("first_name LIKE ?", "%"+search+"%").Or("last_name like ?", "%"+search+"%").Or("surname like ?", "%"+search+"%").Or("identification_no like ?", "%"+search+"%").Find(&resp)
		//db.Find(&resp)
		w.Header().Set("content-Type", "application/json")
		json.NewEncoder(w).Encode(resp)
	} else {
		err := "Invalid argument in Search value"
		http.Error(w, err, 500)
		return
	}

}

func GetCustomerbyId(w http.ResponseWriter, r *http.Request) {
	respo := data.Customer{}
	// var cust = CustomerResp{}
	db := r.Context().Value("database").(*gorm.DB)
	search := r.URL.Query().Get("id")
	//search, _ := uuid.FromString(id)

	if err := db.Where("id = ?", search).Find(&respo).Error; err != nil {
		http.Error(w, "customer not found", 200)
		return

	}

	w.Header().Set("content-Type", "application/json")
	json.NewEncoder(w).Encode(respo)

}
func GetCustomerbyPhone(w http.ResponseWriter, r *http.Request) {
	//respo := data.Customer{}
	response := customerResp{}
	customer := data.Customer{}
	db := r.Context().Value("database").(*gorm.DB)
	search := r.URL.Query().Get("phoneNo")
	if search == "" {
		http.Error(w, "put a valid argument for phone number", 500)
		return
	}
	phone := "{" + search + "}"
	if err := db.Where("phone_nos @> ?", phone).Find(&customer).Error; err != nil {
		response.Errors = append(response.Errors, err.Error())
		w.Header().Set("content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return

	}
	response.CustomerID = customer.Id.String()
	w.Header().Set("content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}
func GetLanguages(w http.ResponseWriter, r *http.Request) {
	lang := []data.Language{}
	db := r.Context().Value("database").(*gorm.DB)
	db.Find(&lang)
	w.Header().Set("Content-Type", "Application/json")
	json.NewEncoder(w).Encode(lang)
}
