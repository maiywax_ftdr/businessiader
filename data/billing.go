package data

import "time"

type Billing struct {
	Id       int    `gorm:"column:id"`
	Code     string `gorm:"column:code"`
	Name     string `gorm:"column:name"`
	Url      string `gorm:"column:url"`
	Username string `gorm:"column:username"`
	Password string `gorm:"column:password"`
	ApiKey   string `gorm:"column:api_key"`

	Description string    `gorm:"column:description"`
	Status      int       `gorm:"column:status"`
	DateCreated time.Time `gorm:"column:date_created_utc"`
}

func (Billing) TableName() string {
	return "public.endpoints"
}
