package data

import (
	"github.com/google/uuid"
)

type Customer struct {
	Id               uuid.UUID `gorm:"column:id"`
	FirstName        string    `gorm:"column:first_name"`
	LastName         string    `gorm:"column:last_name"`
	Surname          string    `gorm:"column:surname"`
	Village          string    `gorm:"column:village"`
	House            string    `gorm:"column:house_no"`
	IdentificationNo string    `gorm:"column:identification_no"`
	PhoneNos         string    `gorm:"column:phone_nos"`
	Gender           string    `gorm:"column:gender"`
	Language         int       `gorm:"column:language_id"`
	//	AlternatePhone   string    `gorm:"column:alternatepPhone`
	Dob string `gorm:"column:dob`
}

func (Customer) TableName() string {
	return "public.customers"
}
