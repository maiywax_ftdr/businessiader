package data

type Language struct {
	Lang_Id  int    `gorm:"column:id"`
	Language string `gorm:"column:language"`
}

func (Language) TableName() string {
	return "public.language"
}
